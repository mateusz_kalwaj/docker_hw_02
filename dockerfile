#docker build -t codementors/word-api-client:1.0.0 .


FROM ubuntu:17.10

ENV WORDART_DIR /opt/codementors/wordart
ENV WORDART_API_USER user
ENV WORDART_API_PASSWORD pass

RUN apt-get update
RUN apt-get install -y curl
RUN mkdir -p $WORDART_DIR


COPY ./entrypoint.sh /

RUN chmod +x entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

